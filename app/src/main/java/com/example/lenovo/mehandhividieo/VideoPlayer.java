package com.example.lenovo.mehandhividieo;

import android.app.Dialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.view.View;
import android.view.Window;
import android.widget.Toast;

import com.google.android.youtube.player.YouTubeBaseActivity;
import com.google.android.youtube.player.YouTubeInitializationResult;
import com.google.android.youtube.player.YouTubePlayer;
import com.google.android.youtube.player.YouTubePlayerView;

import java.security.Provider;

public class VideoPlayer extends YouTubeBaseActivity implements YouTubePlayer.OnInitializedListener {
    public static final String API_KEY = "AIzaSyC74sokNABjm1ctP5qVoV-Fcxu66gsIqSk";
    private static final int RECOVERY_REQUEST = 1;
    public static String VIDEO_ID = "LS4ba1MZ77g";
    private MyPlaybackEventListener playbackEventListener;
    private YouTubePlayer player;
    private MyPlayerStateChangeListener playerStateChangeListener;
    private YouTubePlayerView youTubeView;
    Dialog dialog;
    boolean isfirest = true;
    SharedPreferences pref;

    @Override
    public void onInitializationSuccess(YouTubePlayer.Provider provider, YouTubePlayer youTubePlayer, boolean b) {
        player = youTubePlayer;
        player.setPlayerStateChangeListener(this.playerStateChangeListener);
        player.setPlaybackEventListener(this.playbackEventListener);
        if (!b) {
            player.cueVideo(VIDEO_ID);
        }

    }

    @Override
    public void onInitializationFailure(YouTubePlayer.Provider provider, YouTubeInitializationResult youTubeInitializationResult) {


        if (youTubeInitializationResult.isUserRecoverableError()) {
            youTubeInitializationResult.getErrorDialog(this, 1).show();
            return;
        }
        Toast.makeText(this, "youtubeerror", Toast.LENGTH_SHORT).show();
    }


    private final class MyPlaybackEventListener implements YouTubePlayer.PlaybackEventListener {
        private MyPlaybackEventListener() {
        }

        public void onPlaying() {
        }

        public void onPaused() {
        }

        public void onStopped() {
        }

        public void onBuffering(boolean b) {
        }

        public void onSeekTo(int i) {
        }
    }

    private final class MyPlayerStateChangeListener implements YouTubePlayer.PlayerStateChangeListener {
        private MyPlayerStateChangeListener() {
        }

        public void onLoading() {
        }

        public void onLoaded(String s) {
        }

        public void onAdStarted() {
        }

        public void onVideoStarted() {
        }

        public void onVideoEnded() {

            if(isfirest==true){
                customDailog();
            }
        }

        public void onError(YouTubePlayer.ErrorReason errorReason) {
        }
    }

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_video);
        Bundle extras = getIntent().getExtras();
        if (extras != null) {
            VIDEO_ID = extras.getString("VID");
        }
        pref = PreferenceManager.getDefaultSharedPreferences(this);
        isfirest = pref.getBoolean("RATE", true);
        youTubeView = (YouTubePlayerView) findViewById(R.id.youtubeplayerview);
        youTubeView.initialize(API_KEY, this);
        playerStateChangeListener = new MyPlayerStateChangeListener();
        playbackEventListener = new MyPlaybackEventListener();
    }

    public void customDailog() {

// custom dialog

        dialog = new Dialog(VideoPlayer.this, android.R.style.Theme_NoTitleBar_Fullscreen);
        dialog.getWindow().requestFeature(Window.FEATURE_NO_TITLE);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.setContentView(R.layout.dialogbox);

        dialog.findViewById(R.id.no).setOnClickListener(new View.OnClickListener() {

            @Override

            public void onClick(View v) {
                SharedPreferences.Editor editor = pref.edit();
                editor.putBoolean("RATE", true);
                editor.commit();
                dialog.dismiss();

            }

        });

        dialog.findViewById(R.id.yes).setOnClickListener(new View.OnClickListener() {

            @Override

            public void onClick(View v) {

                SharedPreferences.Editor editor = pref.edit();
                editor.putBoolean("RATE", false);
                editor.commit();
                dialog.dismiss();

            }

        });

        dialog.setCancelable(false);

        dialog.show();

    }

    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == 1) {
            getYouTubePlayerProvider().initialize(API_KEY, VideoPlayer.this);
        }
    }

    protected YouTubePlayerView getYouTubePlayerProvider() {
        return this.youTubeView;
    }

    private void showMessage(String message) {
        Toast.makeText(this, message, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }
}
