package com.example.lenovo.mehandhividieo;

import android.graphics.Bitmap;

public class RowItem {
    private String desc;
    private String imageId;
    private String name;

    public RowItem(String imageId, String name, String desc) {
        this.imageId = imageId;
        this.name = name;
        this.desc = desc;
    }

    public RowItem(Bitmap map) {
    }

    public String getImageId() {
        return this.imageId;
    }

    public void setImageId(String imageId) {
        this.imageId = imageId;
    }

    public String getDesc() {
        return this.desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }

    public String getName() {
        return this.name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String toString() {
        return this.desc;
    }
}
