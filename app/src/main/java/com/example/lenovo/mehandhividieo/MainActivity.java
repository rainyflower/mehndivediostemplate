package com.example.lenovo.mehandhividieo;

import android.app.Activity;
import android.app.Dialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.widget.TextView;

public class MainActivity extends Activity {
    Dialog   dialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_main_menu);


        findViewById(R.id.basic).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(getApplicationContext(), ContentActivity.class);
                i.putExtra("category", "Basic");
                startActivity(i);


            }
        });

        findViewById(R.id.shae).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    String string = "MehandhiVidieo  - \n https://play.google.com/store/apps/details?id=" + getApplicationContext().getPackageName();
                    Intent messageIntent = new Intent("android.intent.action.SEND");
                    messageIntent.setType("text/plain");
                    messageIntent.putExtra("android.intent.extra.SUBJECT","MehandhiVidieo");
                    messageIntent.putExtra("android.intent.extra.TEXT", string);
                    startActivity(Intent.createChooser(messageIntent, "Choose One"));
                } catch (Exception e) {
                    e.printStackTrace();
                }

            }
        });
        findViewById(R.id.rate).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                    startActivity(new Intent(Intent.ACTION_VIEW,
                            Uri.parse("https://play.google.com/store/apps/details?id=" + getApplicationContext().getPackageName())));


            }
        });

        findViewById(R.id.advancedclases).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent i = new Intent(getApplicationContext(), ContentActivity.class);
                i.putExtra("category", "Advance");
                startActivity(i);

            }
        });

        findViewById(R.id.profestional).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent i = new Intent(getApplicationContext(), ContentActivity.class);
                i.putExtra("category", "Professional");
                startActivity(i);
            }
        });



}
    public void customDailog() {

// custom dialog

        dialog = new Dialog(MainActivity.this, android.R.style.Theme_NoTitleBar_Fullscreen);
        dialog.getWindow().requestFeature(Window.FEATURE_NO_TITLE);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.setContentView(R.layout.dialogbox);
        TextView title=(TextView)dialog.findViewById(R.id.head) ;
        title.setText("Exit");
        TextView text=(TextView)dialog.findViewById(R.id.text) ;
        text.setText("Do You Want Exit?");
        dialog.findViewById(R.id.no).setOnClickListener(new View.OnClickListener() {

            @Override

            public void onClick(View v) {

                dialog.dismiss();

            }

        });

        dialog.findViewById(R.id.yes).setOnClickListener(new View.OnClickListener() {

            @Override

            public void onClick(View v) {


                dialog.dismiss();
                finish();

            }

        });

        dialog.setCancelable(false);

        dialog.show();

    }


    @Override
    public void onBackPressed() {
        customDailog();



    }
}
