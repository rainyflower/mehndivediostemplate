package com.example.lenovo.mehandhividieo;

import android.app.Activity;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;

import java.util.ArrayList;

/**
 * Created by MM 12 on 12-Jul-17.
 */

public class CustomList extends BaseAdapter {

    // Declare variables
    Activity activity;
    ArrayList<String> imageurls, descreptions;
    String level;

    Context context;

    public CustomList(Context a,String level, ArrayList<String> imageurls, ArrayList<String> desccreption) {
        context = a;
        this.imageurls = imageurls;
        this.descreptions = desccreption;
        this.level=level;
    }

    public int getCount() {

        return descreptions.size();

    }

    public Object getItem(int position) {
        return position;
    }

    public long getItemId(int position) {
        return position;
    }

    public View getView(int position, View convertView, ViewGroup parent) {

        // TODO Auto-generated method stub

        LayoutInflater layout = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        if (convertView == null) {
            convertView = new View(context);
            convertView = layout.inflate(R.layout.list_item, null);
        }
        TextView txtDesc = (TextView) convertView.findViewById(R.id.desc);
        txtDesc.setTextColor(Color.WHITE);
        TextView txtName = (TextView) convertView.findViewById(R.id.name);
        txtName.setText(level);
        ImageView imageView = (ImageView) convertView.findViewById(R.id.icon);
        txtDesc.setText(descreptions.get(position));

        Glide.with(context).load(imageurls.get(position))
                .thumbnail(0.5f)
                .crossFade()
                .diskCacheStrategy(DiskCacheStrategy.ALL)
                .into(imageView);

        return convertView;
    }

}

