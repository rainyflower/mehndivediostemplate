package com.example.lenovo.mehandhividieo;

import android.app.Activity;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;

public class ContentActivity extends Activity {
    String category;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_content);
        Bundle extras = getIntent().getExtras();
        if (extras != null) {
            category = extras.getString("category");
        }


        findViewById(R.id.next).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(getApplicationContext(), VideoListActivity.class);
                i.putExtra("category",category);
                startActivity(i);


            }
        });

    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
finish();
    }
}
